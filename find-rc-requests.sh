#!/bin/bash

set -eu

LORE_DIR="/data/lore-stable.git"
KERNEL_DIR="/data/linux-stable-rc.git"
GITLAB_MIRROR_URL="https://gitlab-ci-token:${TOKEN_GITLAB_MIRROR}@gitlab.com/Linaro/lkft/mirrors/stable/linux-stable-rc.git"

# Git doesn't like the ownership mismatch (due to Docker runner)
git config --global --add safe.directory "${LORE_DIR}"
git config --global --add safe.directory "${KERNEL_DIR}"

if [ -v CI ]; then
  WORKDIR="${CI_PROJECT_DIR}/workspace"
else
  WORKDIR="$(dirname "$(readlink -e "$0")")/workspace"
fi
mkdir -p "${WORKDIR}"

file_lore_last="${WORKDIR}/last-commit-lore.txt"
file_trigger_last="${WORKDIR}/last-trigger.txt"

# $1: number of arguments
# $2...: arguments
protect_args() {
  if [ $# -lt 1 ]; then
    return 1
  fi
  num_args=$1
  shift
  declare -a args
  args=("$@")

  if [ "${#args[@]}" -lt "${num_args}" ]; then
    echo "Not enough arguments provided (${#args[@]}) to ${FUNCNAME[1]}: Need ${num_args} arguments."
    return 1
  fi

  return 0
}

# $1: commit
save_last_lore() {
  protect_args 1 "$@" || return 1

  commit="$1"

  echo "Saving ${commit} as last lore."
  echo "${commit}" > "${file_lore_last}"
}

# $1: commit
save_last_trigger() {
  protect_args 1 "$@" || return 1

  commit="$1"

  echo "Saving ${commit} as last trigger."
  echo "${commit}" > "${file_trigger_last}"
}

# $1: commit
# $2: email header (including colon)
get_from_message() {
  protect_args 2 "$@" || return 1

  git -C "${LORE_DIR}" grep "^$2" "${commit}:m" | cut -d: -f4- | sed -e 's:^ ::'
}

# $1: commit
trigger_pipeline_stable_rc() {
  protect_args 1 "$@" || return 1

  save_last_lore "${commit}"

  kernel_tree="$(get_from_message "${commit}" "X-KernelTest-Tree:")"
  kernel_branch="$(get_from_message "${commit}" "X-KernelTest-Branch:")"
  kernel_version="$(get_from_message "${commit}" "X-KernelTest-Version:")"

  echo "Triggering pipeline for ${kernel_version}..."
  git -C "${KERNEL_DIR}" fetch "${kernel_tree}" "${kernel_branch}"
  git -C "${KERNEL_DIR}" push "${GITLAB_MIRROR_URL}" "+FETCH_HEAD:${kernel_branch}"

  save_last_trigger "${commit}"
}

# $1: commit
trigger_pipeline_stable() {
  protect_args 1 "$@" || return 1

  save_last_lore "${commit}"

  subject="$(get_from_message "${commit}" "Subject:")"
  kernel_version="$(echo "${subject}" | cut -d: -f4- | grep -Eo '[456]\.[0-9]*\.[0-9]*')"
  major_minor="$(echo "${kernel_version}" | cut -d. -f 1,2)"
  kernel_branch="linux-${major_minor}.y"

  echo "Triggering pipeline for ${kernel_version}..."
  git -C "${KERNEL_DIR}" fetch --tags https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git "${kernel_branch}"
  git -C "${KERNEL_DIR}" push --tags "${GITLAB_MIRROR_URL}" "+FETCH_HEAD:${kernel_branch}"

  save_last_trigger "${commit}"
}

# $1: commit
is_greg_or_sasha() {
  commit="$1"
  result=1
  if git -C "${LORE_DIR}" grep -q ^'From: Greg Kroah-Hartman <gregkh@linuxfoundation.org>' "${commit}:m" ||
     git -C "${LORE_DIR}" grep -q ^'From: Sasha Levin <sashal@kernel.org>' "${commit}:m"; then
       result=0
  fi
  return "${result}"
}

if [ -v VERBOSE ]; then
  set -x
fi

# Update Lore
git -C "${LORE_DIR}" fetch origin master:master

# If first run, then save last Lore commit and exit
if [ ! -f "${file_lore_last}" ]; then
  echo "NOTE: No previous commit"
  commit="$(git -C "${LORE_DIR}" rev-parse master)"
  save_last_lore "${commit}"
  exit 0
fi

if [ ! -v LORE_LAST_COMMIT ]; then
  LORE_LAST_COMMIT="$(head -n1 "${file_lore_last}")"
fi

echo "Previous last Lore: ${LORE_LAST_COMMIT}"

ctr=0
while read -r commit; do
  # Is the mail an RC review request?
  if git -C "${LORE_DIR}" grep -qE '^Subject: \[PATCH\ [456]\.[0-9]?[0-9]*\ 0[0]\/*' "${commit}:m"; then
    if git -C "${LORE_DIR}" grep -q "^X-KernelTest-Version:" "${commit}:m"; then
      # Initiate testing!
      echo
      echo "Lore commit is review request: ${commit}"
      trigger_pipeline_stable_rc "${commit}"
    fi
  elif \
  git -C "${LORE_DIR}" grep -q ^'Subject: Linux [456]\.[0-9]*\.[0-9]*$' "${commit}:m" && \
  is_greg_or_sasha "${commit}"; then
    # Initiate testing!
    echo
    echo "Lore commit of release: ${commit}"
    trigger_pipeline_stable "${commit}"
  fi
  ctr=$((ctr+1))
done < <(git -C "${LORE_DIR}" log --reverse --format=%H "${LORE_LAST_COMMIT}..master" | awk '{print $1}')

echo "Processed ${ctr} messages"

commit="$(git -C "${LORE_DIR}" rev-parse master)"
save_last_lore "${commit}"

LORE_LAST_COMMIT="$(head -n1 "${file_lore_last}")"
echo "New last Lore:      ${LORE_LAST_COMMIT}"
